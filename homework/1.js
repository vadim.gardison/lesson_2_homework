var bodyEl = document.getElementsByTagName('body');
var buttCont = document.getElementById('buttonContainer');
var tabCont = document.getElementById('tabContainer');
var elementsButttCont = buttCont.querySelectorAll('.showButton');
var elementsTabCont = tabCont.querySelectorAll('.tab');
var hideShowButton = document.createElement('button');
var chekActive;
hideShowButton.innerText = 'Hide';
hideShowButton.style.width = '100%';
buttCont.style.display = 'flex';
bodyEl[0].insertBefore(hideShowButton, buttCont);
elementsButttCont.forEach(function(item){
  item.onclick = function() {
    if (chekActive !== undefined) {
      elementsTabCont[chekActive].classList.remove('active');
    }
    elementsTabCont.forEach(function(itemTab, i) {
      if (item.dataset.tab == itemTab.dataset.tab) {
        itemTab.classList.add('active');
        chekActive = i;
      }
    })
  }
})

hideShowButton.onclick = function() {
  hideShowButton.innerText = hideShowButton.innerText == 'Show' ? 'Hide' : 'Show';
  buttCont.style.display = buttCont.style.display == 'flex' ? 'none' : 'flex';
  elementsTabCont.forEach(function(itemTab, i) {
      itemTab.classList.remove('active');
  })
}
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
